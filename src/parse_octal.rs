use core::ops::{Add, Mul};

#[derive(Debug)]
pub enum OctalParserError {
    InvalidDigit(u8),
}

pub fn from_octal_ascii<T>(src: &[u8]) -> Result<T, OctalParserError>
where
    T: Default + From<u8> + Add<Output = T> + Mul<Output = T>,
{
    let mut val: T = T::default();
    for &c in src {
        if c > b'7' || c < b'0' {
            return Err(OctalParserError::InvalidDigit(c));
        }
        let x = c - b'0';
        val = (val * 8.into()) + x.into();
    }
    Ok(val)
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test() {
        assert!(matches!(from_octal_ascii(b"000"), Ok(0u16)));
        assert!(matches!(from_octal_ascii(b"001"), Ok(1u16)));
        assert!(matches!(from_octal_ascii(b"010"), Ok(8u16)));

        assert!(matches!(from_octal_ascii(b"077"), Ok(0o77u8)));
    }
}
