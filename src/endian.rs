use std::str::FromStr;

use byteorder::{LittleEndian, BigEndian, ByteOrder};


#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Endian {
    Little,
    Big,
}

impl FromStr for Endian {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s.to_lowercase() as &str {
            "big" => Ok(Self::Big),
            "little" => Ok(Self::Little),
            _ => Err("Invalid Endianness".to_string())
        }
    }
}

impl Endian {
    pub fn write_u16(&self, buf: &mut [u8], val: u16) {
        match self {
            Endian::Little => LittleEndian::write_u16(buf, val),
            Endian::Big => BigEndian::write_u16(buf, val),
        }
    }
}

#[cfg(test)]
mod test{

    use super::*;

    #[test]
    fn test_from_str() {
        assert_eq!(Endian::Big, Endian::from_str("big").unwrap());
        assert_eq!(Endian::Little, Endian::from_str("little").unwrap());

        assert_eq!(Endian::Big, Endian::from_str("Big").unwrap());
        assert_eq!(Endian::Big, Endian::from_str("BIG").unwrap());
        assert_eq!(Endian::Big, Endian::from_str("bIg").unwrap());

        assert_eq!(Endian::Little, Endian::from_str("Little").unwrap());
        assert_eq!(Endian::Little, Endian::from_str("LITTLE").unwrap());
        assert_eq!(Endian::Little, Endian::from_str("lITtlE").unwrap());
    }

    #[test]
    #[cfg(target_endian="little")]
    fn test_write() {
        let mut buf = [0; 2];
        Endian::Little.write_u16(&mut buf, 0xaa55);
        assert_eq!(buf, [0x55, 0xaa]);

        let mut buf = [0; 2];
        Endian::Big.write_u16(&mut buf, 0xaa55);
        assert_eq!(buf, [0xaa, 0x55]);
    }
}
