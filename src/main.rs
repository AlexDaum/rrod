use std::{
    cmp::min,
    io::{self, stdin, stdout, BufRead, BufReader, BufWriter, Write},
};

use argparse::ArgumentParser;
use endian::Endian;

use parse_octal::from_octal_ascii;

mod endian;
mod parse_octal;

const DATA_PER_LINE: usize = 16;

#[derive(Debug, PartialEq, Eq)]
struct ODLine {
    offset: usize,
    data: [u8; DATA_PER_LINE],
}

#[derive(Debug, PartialEq, Eq)]
enum ODLineResult {
    Data(ODLine),
    RepeatLast,
}

impl ODLine {
    /// print the binary value of the line to the writer.
    /// bytes defines how many bytes of the line will be written, if it is higher than
    /// DATA_PER_LINE, then the line will be repeated until enough bytes are written.
    pub fn print(&self, bytes: usize, writer: &mut impl Write) -> io::Result<()> {
        let mut left = bytes;
        while left > 0 {
            let to_write = min(left, DATA_PER_LINE);
            writer.write(&self.data[0..to_write])?;
            left -= to_write;
        }
        Ok(())
    }

    /// Parses a ODLine. This assumes a OD Format with no Repetition.
    pub fn parse(line: &[u8], endian: Endian) -> Self {
        let mut parts = line.split(|b| *b == b' ');
        let offset = parts.next().expect("Invalid Format: No offset");
        let offset = from_octal_ascii(offset).expect("Offset is not an octal string");

        let mut data = [0; DATA_PER_LINE];

        parts
            .map(|w| from_octal_ascii(w).expect("Not a valid octal String"))
            .enumerate()
            .for_each(|(idx, val)| endian.write_u16(&mut data[(2 * idx)..(2 * (idx + 1))], val));

        ODLine { offset, data }
    }
}

fn parse_od_line(line: &[u8], endian: Endian) -> ODLineResult {
    if line.contains(&b'*') {
        // if it contains a star: repeat
        ODLineResult::RepeatLast
    } else {
        ODLineResult::Data(ODLine::parse(line, endian))
    }
}

fn parse_args() -> Endian {
    use argparse::Store;

    let mut endian = Endian::Little;
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Reverses the output of od (only the default format is supported)");
        ap.refer(&mut endian)
            .add_option(&["--endian"], Store, "Endianness of the input");
        ap.parse_args_or_exit();
    }

    endian
}

fn main() -> io::Result<()> {
    let endian = parse_args();

    let mut inreader = BufReader::with_capacity(128 * 1024, stdin());
    let mut outwriter = BufWriter::with_capacity(128 * 1024, stdout());
    let mut last_line: Option<ODLine> = None;

    let mut line = Vec::new();

    while inreader.read_until(b'\n', &mut line)? > 0 {
        // last char \n needs to be truncated
        let next_line = parse_od_line(&line[..line.len() - 1], endian);

        match next_line {
            ODLineResult::Data(d) => {
                if let Some(last) = last_line {
                    last.print(d.offset - last.offset, &mut outwriter)?;
                }
                last_line = Some(d);
            }
            ODLineResult::RepeatLast => (),
        }
        line.clear();
    }

    outwriter.flush().unwrap();

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parseline() {
        assert_eq!(
            parse_od_line(b"0014 062510 066154", Endian::Little),
            ODLineResult::Data(ODLine {
                offset: 0o14,
                data: [b'H', b'e', b'l', b'l', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            })
        );
        assert_eq!(parse_od_line(b"*", Endian::Little), ODLineResult::RepeatLast);
    }
}
